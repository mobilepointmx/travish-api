/**
 * Created by IvanIsrael on 02/09/2015.
 */
"use strict"
module.exports = function(sequelize, DataTypes) {
    var Client = sequelize.define("Client", {
        name: {type: DataTypes.STRING, unique: true, allowNull: false},
        id: {type: DataTypes.STRING, unique: true, primaryKey: true},
        clientSecret: {type: DataTypes.STRING, allowNull: true}
    }, {
        classMethods: {
            associate: function(models) {
                Client.hasMany(models.AccessToken, {
                    onDelete: 'cascade'

                });
                Client.hasMany(models.RefreshToken, {
                    onDelete: 'cascade'

                });
            }
        }
    });

    return Client;
};

