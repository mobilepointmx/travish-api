/**
 * Created by IvanIsrael on 02/09/2015.
 */
"use strict";

var crypto = require('crypto');
var config = require('../config/config.json');

module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define("User", {
        username: {
            type: DataTypes.STRING(100),
            unique: true,
            allowNull: false,
            validate: {
                isEmail: true,
                notEmpty: true
            }
        },
        password: DataTypes.STRING(100),
        role: {
            type:   DataTypes.ENUM,
            values: ['ADMIN', 'NORMAL']
        },
        phone:{
            type: DataTypes.STRING(50)
        }
    }, {
        classMethods: {
            associate: function(models) {
                User.hasMany(models.AccessToken, {
                    onDelete: 'cascade'

                });
                User.hasMany(models.RefreshToken, {
                    onDelete: 'cascade'
                });
                User.hasMany(models.Schedule, {
                    onDelete: 'cascade'
                });
            },
            encryptPassword: function(password) {
                    return crypto.createHmac('sha1', config.salt).update(password).digest('hex');
                    //more secure - return crypto.pbkdf2Sync(password, this.salt, 10000, 512);
                },
            checkPassword: function(password) {
                return this.encryptPassword(password) === this.password;
            }
        }
    });

    return User;
};