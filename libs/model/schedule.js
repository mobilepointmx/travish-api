/**
 * Created by IvanIsrael on 04/09/2015.
 */
module.exports = function(sequelize, DataTypes) {
    var Schedule = sequelize.define("Schedule", {
        meeting_datetime:{
            type: DataTypes.DATE
        },
        luggage_amount: {
            type: DataTypes.INTEGER
        },
        reservation_date: {
            type: DataTypes.DATE
        },
        status: {
            type: DataTypes.STRING
        }

    });
    return Schedule;
};