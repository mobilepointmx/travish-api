/**
 * Created by IvanIsrael on 02/09/2015.
 */
"use strict"
module.exports = function(sequelize, DataTypes) {
    var AccessToken = sequelize.define("AccessToken", {
        token: {type: DataTypes.STRING, allowNull: false}
    });

    return AccessToken;
};