/**
 * Created by IvanIsrael on 04/09/2015.
 */
module.exports = function(sequelize, DataTypes) {
    var Flight = sequelize.define("Flight", {
        airportName:{
            type: DataTypes.STRING(150)
        },
        airlineName: {
            type: DataTypes.STRING(100)
        },
        terminal: {
            type: DataTypes.STRING(40)
        },
        departureDate: {
            type: DataTypes.DATE
        },
        flightNumber: {
            type: DataTypes.STRING(10)
        }

    }, {
        classMethods: {
            associate: function (models) {
                Flight.belongsTo(models.Schedule, {
                    onDelete: 'cascade'

                });
            }
        }
    });
    return Flight;
};