var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var ClientPasswordStrategy = require('passport-oauth2-client-password').Strategy;
var BearerStrategy = require('passport-http-bearer').Strategy;

var libs = '../';

var config    = require(libs +'config/config.json');
var User = require(libs + 'model/user');
var Client = require(libs + 'model/client');
var AccessToken = require(libs + 'model/accessToken');
var RefreshToken = require(libs + 'model/refreshToken');
var models = require(libs+ 'model');

passport.use(new BasicStrategy(

    function(username, password, done) {
        console.log(models);
        models.Client.findOne({
            where: {clientId: username},
        })
            .then(function(client) {
            // project will be the first entry of the Projects table with the title 'aProject' || null
            // project.title will contain the name of the project
                if (!client) {
                    return done(null, false);
                }
                if (!client) {
                    return done(null, false);
                }
                if (client.clientSecret !== password) {
                    return done(null, false);
                }
            return done(null, client);
        })
            .catch(function(err){
                return done(err);
        });
    }
));

passport.use(new ClientPasswordStrategy(
    function(clientId, clientSecret, done) {
        console.log(models + '2');
        models.Client.findOne({
            where: {id: clientId,
            clientSecret:clientSecret},
        })
            .then(function(client) {

                if(!client)
                    return done(null, false);
                if(client.clientSecret !== clientSecret)
                    return done(null, false);
                console.log("si lo encontro client");
                return done(null, client);
        })
            .catch(function (err) {
                log.error('Connection error:', err);
                return done(null, false);
        });
    }
));

passport.use(new BearerStrategy(
    function(accessToken, done) {
        console.log(models+'3');

        models.AccessToken
            .findOne({
            where: {token: accessToken}
            })
            .then(function(token){
                if (!token) {
                    return done(null, false);
                }
                if( Math.round((Date.now()-token.createdAt)/1000) > config.security.tokenLife ) {

                    models.AccessToken.destroy({ where:{token: accessToken} })
                        .then(function(){})
                        .catch(function(err){
                            return done(err);
                        });

                    return done(null, false, { message: 'Token expired' });
                }
                models.User.findById(token.UserId)
                    .then(function(user) {
                        if (!user) {
                            return done(null, false, { message: 'Unknown user' });
                        }
                        var info = { scope: '*' };
                        return done(null, user, info);
                    })
                    .catch(function (err) {
                        log.error('Connection error:', err);
                        return done(err);
                    });
            })
            .catch(function(err){
                return done(err);
            });
    }
));