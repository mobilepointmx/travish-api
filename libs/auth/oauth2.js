var oauth2orize = require('oauth2orize');
var passport = require('passport');
var crypto = require('crypto');

var libs = '../';

var config    = require(libs +'config/config.json');
var log = require(libs + 'log')(module);

//var db = require(libs + 'db/mysql');
//var User = require(libs + 'model/user');
//var AccessToken = require(libs + 'model/accessToken');
//var RefreshToken = require(libs + 'model/refreshToken');
var models = require(libs + "model");
// create OAuth 2.0 server
var aserver = oauth2orize.createServer();

// Generic error handler
var errFn = function (cb, err) {
	if (err) { 
		return cb(err); 
	}
};

// Destroys any old tokens and generates a new access and refresh token
var generateTokens = function (data, done) {
	console.log(data);
	// curries in `done` callback so we don't need to pass it
    var errorHandler = errFn.bind(undefined, done), 
	    refreshToken,
	    refreshTokenValue,
	    token,
	    tokenValue;
	console.log("DATA: " + data)
	models.RefreshToken.destroy({
		where: data
	})
		.then(function(rt) {
			console.log("rt: " +rt);
	})
		.catch(function (err) {
		log.error('Connection error:', err);
		errorHandler();
	});
	models.AccessToken.destroy({
		where: data
	})
		.then(function(rt) {
	})
		.catch(function (err) {
		log.error('Connection error:', err);
		errorHandler();
	});
    tokenValue = crypto.randomBytes(32).toString('hex');
    refreshTokenValue = crypto.randomBytes(32).toString('hex');

    data.token = tokenValue;

	models.AccessToken
		.create(data)
		.then(function(access) {
		})
		.catch(function(err){
			errorHandler();
		});

    data.token = refreshTokenValue;
	models.RefreshToken
		.create(data)
		.then(function(access) {
			done(null, tokenValue, refreshTokenValue, {
				'expires_in': config.security.tokenLife});
		})
		.catch(function(err){
			errorHandler();
		});
};

// Exchange username & password for access token.
aserver.exchange(oauth2orize.exchange.password(function(client, email, password, scope, done) {
	console.log("buscando usuario...");

	models.User.findOne({
		where: {username: email}
	}).then(function(user) {
		// project will be the first entry of the Projects table with the title 'aProject' || null
		// project.title will contain the name of the project
		console.log("si lo encontro " +models.User.checkPassword(password));
		console.log("pass " +user.password);
		if (!user || models.User.checkPassword(password)) {
			return done(null, false);
		}
		var model = {
			UserId: user.id,
			ClientId: client.id
		};
		generateTokens(model, done);

	}).catch(function (err) {
		log.error('Connection error:', err);
		return done(err);
	});
}));

// Exchange refreshToken for access token.
aserver.exchange(oauth2orize.exchange.refreshToken(function(client, refreshToken, scope, done) {

	models.RefreshToken.findOne({
		where: {token: refreshToken, clientId: client.clientId},
	})
		.then(function(token) {
			// project will be the first entry of the Projects table with the title 'aProject' || null
			// project.title will contain the name of the project
			if (!token) {
				return done(null, false);
			}
			models.User.findById(token.userId)
				.then(function(user) {
					if (!user) { return done(null, false); }
					var model = {
						UserId: user.userId,
						ClientId: client.id
					};

					generateTokens(model, done);
				})
				.catch(function (err) {
					log.error('Connection error:', err);
					return done(err);
				});

		})
		.catch(function (err) {
			log.error('Connection error:', err);
			return done(err);
		});

}));

// token endpoint
//
// `token` middleware handles client requests to exchange authorization grants
// for access tokens.  Based on the grant type being exchanged, the above
// exchange middleware will be invoked to handle the request.  Clients must
// authenticate when making requests to this endpoint.

exports.token = [
	passport.authenticate(['basic', 'oauth2-client-password'], { session: false }),
	aserver.token(),
	aserver.errorHandler()
];
