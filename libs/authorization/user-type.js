/**
 * Created by IvanIsrael on 03/09/2015.
 */
module.exports ={
    ensureAdmin: function(req, res, next) {
        // ensure authenticated user exists with admin role,
        // otherwise send 401 response status
        if (req.user && req.user.role == 'ADMIN') {
            return next();
        } else {
            return res.send(401);
        }
    },
    ensureUser: function(req, res, next) {
        // ensure authenticated user exists with admin role,
        // otherwise send 401 response status
        if (req.user && req.user.role == 'NORMAL') {
            return next();
        } else {
            return res.send(401);
        }
    },
}