/**
 * Created by IvanIsrael on 02/09/2015.
 */

"use strict";

module.exports = function(sequelize, DataTypes) {
    var Task = sequelize.define("Task", {
        title: DataTypes.STRING,
        UserId: {type: DataTypes.INTEGER, unique: true}
    }, {
        classMethods: {
            associate: function(models) {
                Task.belongsTo(models.User, {
                    foreignKeyConstraint: true,
                unique: true,

                    foreignKey: {
                        allowNull: false
                    }

                });
            }
        },
        constraints: false
    });

    return Task;
};