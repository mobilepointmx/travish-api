/**
 * Created by Ivan Sabido on 01/09/2015.
 */
"use strict";
var libs = process.cwd() ;

var log = require('../log')(module);
var Sequelize = require('sequelize');

module.exports = function(){
    console.log("En la funcion");
    var sequelize = new Sequelize('travish', 'root', '12345678', {
        host: "127.0.0.1",
        port: 3306,
        dialect: 'mysql'
    })
        .authenticate()
        .then(function () {
            log.info("Connected to DB!");
        })
        .catch(function (err) {
            log.error('Connection error:', err);
        });
    return sequelize;
}

