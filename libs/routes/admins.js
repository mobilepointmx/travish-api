/**
 * Created by IvanIsrael on 03/09/2015.
 */
var config = require('../config/config.json');
var express = require('express');
var passport = require('passport');
var router = express.Router();
var authorization = require('../authorization/user-type');

var libs = '../';
var log = require(libs + 'log')(module);

var models = require(libs + 'model');

var models = require(libs + 'model');

router.get('/', passport.authenticate('bearer', { session: false }),authorization.ensureAdmin,
    function(req, res) {
        // req.authInfo ofrece información extra del usuario autenticado. Puede usarse para indicar
        // el scope o controlar accesos.

        res.json(req.user);
    }
);

router.post('/',function(req, res) {
        // req.authInfo ofrece información extra del usuario autenticado. Puede usarse para indicar
        // el scope o controlar accesos.
        models.User.create({ username: req.body.username, password: models.User.encryptPassword(req.body.password), phone: req.body.phone, role: 'ADMIN'})
            .then(function(user) {
                res.json({status: 'OK', user: user});
            })
            .catch(function(err){
                res.statusCode = 400;
                res.json({
                    error: err.errors
                });

});
//res.json(req.user);
    }
);

router.put('/',passport.authenticate('bearer', { session: false }),authorization.ensureAdmin, function(req, res) {
        // req.authInfo ofrece información extra del usuario autenticado. Puede usarse para indicar
        // el scope o controlar accesos.
        models.User.update({
                password: models.User.encryptPassword(req.body.password), phone: req.body.phone},
            {
                where:{username:req.user.username}
            })
            .then(function(user) {
                res.statusCode = 201;
                console.log("PUT then");
                res.json({status: 'Created', user: req.body});
            })
            .catch(function(err){
                res.statusCode = 400;
                res.json({
                    error: err.errors
                });

            });
        //res.json(req.user);
    }
);

module.exports = router;