var express = require('express');
var passport = require('passport');
var router = express.Router();
var authorization = require('../authorization/user-type');

var libs = '../';
var log = require(libs + 'log')(module);
var utils = require('../utils/utils');
var models = require(libs + 'model');

router.get('/', passport.authenticate('bearer', { session: false }),authorization.ensureUser,
    function(req, res) {
        // req.authInfo is set using the `info` argument supplied by
        // `BearerStrategy`.  It is typically used to indicate scope of the token,
        // and used in access control checks.  For illustrative purposes, this
        // example simply returns the scope in the response.

        console.log(utils.getDate("2015-09-03 22:36:28"));
        console.log(utils.getTime("2015-09-03 22:36:28"));
        res.json(req.user);
    }
);

router.post('/',function(req, res) {
        // req.authInfo is set using the `info` argument supplied by
        // `BearerStrategy`.  It is typically used to indicate scope of the token,
        // and used in access control checks.  For illustrative purposes, this
        // example simply returns the scope in the response.
        console.log("EN POST / " + req.body.username);
        models.User.create({ username: req.body.username, password: models.User.encryptPassword(req.body.password), phone: req.body.phone, role: 'NORMAL'})
            .then(function(user) {
                console.log("POST then");
                res.json({status: 'OK', user: user});
            })
            .catch(function(err){
                res.statusCode = 400;
                res.json({
                    error: err.errors
                });

            });
        //res.json(req.user);
    }
);

router.put('/',passport.authenticate('bearer', { session: false }),authorization.ensureUser,function(req, res) {
        // req.authInfo is set using the `info` argument supplied by
        // `BearerStrategy`.  It is typically used to indicate scope of the token,
        // and used in access control checks.  For illustrative purposes, this
        // example simply returns the scope in the response.
        console.log("EN PUT / " + req.user.username);

        models.User.update({
           password: models.User.encryptPassword(req.body.password), phone: req.body.phone},
            {
                where:{username:req.user.username}
            })
            .then(function(user) {
                res.statusCode = 201;
                console.log("PUT then");
                res.json({status: 'Created', user: req.body});
            })
            .catch(function(err){
                res.statusCode = 400;
                res.json({
                    error: err.errors
                });

            });
        //res.json(req.user);
    }
);

module.exports = router;