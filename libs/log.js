var winston = require('winston');

winston.emitErrs = true;

function logger(module) {

    return new winston.Logger({
        transports : [
            new winston.transports.File({
                level: 'info',
                filename: '../logs/all.log',
                handleException: true,
                json: true,
                maxSize: 5242880, //5mb 
                maxFiles: 2, 
                colorize: false
            }),
            new winston.transports.Console({
                level: 'debug',
                label: getFilePath(module),
                handleException: true,
                json: false,
                colorize: true
            })
        ],
        exitOnError: false
    });
}

function getFilePath (module ) {
    //using filename in log statements
    return module.filename.split('/').slice(-2).join('/');
}

//logger.log('silly', "127.0.0.1 - there's no place like home");
//logger.log('debug', "127.0.0.1 - there's no place like home");
//logger.log('verbose', "127.0.0.1 - there's no place like home");
//logger.log('info', "127.0.0.1 - there's no place like home");
//logger.log('warn', "127.0.0.1 - there's no place like home");
//logger.log('error', "127.0.0.1 - there's no place like home");
//logger.info("127.0.0.1 - there's no place like home");
//logger.warn("127.0.0.1 - there's no place like home");
//logger.error("127.0.0.1 - there's no place like home");

module.exports = logger;
