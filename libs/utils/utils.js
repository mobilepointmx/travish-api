/**
 * Created by IvanIsrael on 04/09/2015.
 */

module.exports = {
    getTime: function(datetime){
        var date = new Date(datetime);
        return date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    },
    getDate: function(datetime){
        var date = new Date(datetime);
        var monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];
        //return date.toString();
        return monthNames[date.getMonth()] + " " + date.getDate() + " " + date.getFullYear();
    }
};